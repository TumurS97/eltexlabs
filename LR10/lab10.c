#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>	
#include <pthread.h>
/*Противостояние нескольких команд. Каждая команда увеличивается 
на случайное количество бойцов и убивает случайное количество бойцов участника. 
Борьба каждой команды реализуется в отдельном потоке.*/

pthread_mutex_t mutex;
int mas[124];
int x=0;
int num=0;
struct DATA
{
	int i;
};
typedef struct DATA Data;

void *fun(void *args)
{	
	Data *a = (Data*) args;
	while (1)
	{
		pthread_mutex_lock(&mutex);
			
		if(mas[a->i] <= 0)
		{
			pthread_mutex_unlock(&mutex);
			break;
		}
			
	mas[a->i]+=(rand() % 6 +1);
	printf("\nin work p[%d]=%d\n",a->i,mas[a->i]);
	for(int j=0;j<x;j++)
	{
		if(j != a->i)
		{	
			mas[j]-=(rand() % 10 +1);
			if(mas[j]<=0)
			printf("   p[%d]=выбыл (%d)\n",j,mas[j]);
			else
			printf("   p[%d]=%d\n",j,mas[j]);
		}
	}
			
	int c=0;
	for( int p=0;p<x;p++)
	{
		if(mas[p]<=0 )
		c=c+1;
	}
			
	if(c==x-1)
	{
		printf("\n\nпоток номер %d  ПОБЕДА\n\n",a->i);
		c=0;
		pthread_mutex_unlock(&mutex);
		break;
	}
	pthread_mutex_unlock(&mutex);
	sleep(1);
	}
		
	printf("\n---------I[%d] am done-------\n",a->i);
	return NULL;
}

int main (int argc, char **argv)
{
	x=5;
	for(int i=0;i<x;i++)
	{
		mas[i]=20;
	}
	srand(getpid());
	pthread_t threads[x];
	Data data[x];
	pthread_mutex_init(&mutex,NULL);
	
	for(int i=0;i<x;i++)
	{
		data[i].i=i;
		pthread_create(&threads[i],NULL,fun,&data[i]);
	}

	for(int i=0; i<x;i++)
	{
		pthread_join(threads[i],NULL);
	}

	pthread_mutex_destroy(&mutex);
	printf("\n\nРезультат:\n");
	for (int i=0;i<x;i++)
	{
		printf("mas[%d]=%d\n",i,mas[i]);
	}
}
