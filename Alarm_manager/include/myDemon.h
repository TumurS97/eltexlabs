#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include <zlog.h>
#include <pthread.h> 
#include <assert.h>
#include <stdbool.h>
#include "zmq.h"
#define N 5
#define LEN 1024
#define ACK 10
#define NUM_THREAD 2

enum type_message_e
{
    DELETE_ALL = 1,
    READ_ALL,
    FILTR,
    SEND_MES,
};

enum message_status_e
{
    NORMILIZE = 0,
    ALARM,
};

typedef enum filtr_parametrs
{
    FILTR_MODULE = 1,
    FILTR_PRIORITY,
    FILTR_ACTIVE,
} filtr_parametrs_e;

typedef struct my_message
{
    pid_t pid;
    unsigned int priority;
    unsigned int type;
    char mes[LEN];
} my_message_t;



int print_file_mes(FILE *FP, my_message_t * arg, int num);
void *create_recieve_fun(void *args);
void *create_manager_fun(void *args);

int check_the_args(int argc, char **argv);
int create_zlog();

int delete_all();
int write_all();
int write_by_filt(filtr_parametrs_e fild, int value);
int search_active_alarm(my_message_t *arg, int size);
int rcv_mesage(my_message_t *arg);
void print_message(my_message_t *args, unsigned int  i);
int create_connections(void *context, zlog_category_t *logger);
int create_message( my_message_t *arg, char *argv);
int send_message(char *mas);
int close_connections();



