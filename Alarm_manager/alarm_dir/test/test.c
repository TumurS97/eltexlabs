#include <tap.h>
#include <fff.h>
#include "myDemon.h"
#define FILE_NAME "alarm.log"

int start_testing();

int main () 
{
    int return_value = 0;
    plan(4);
    return_value = start_testing();
    if (return_value != 0)
    {
        perror("fail");
        return_value = -1;
        goto exit;
    }

exit:
    done_testing();
    return return_value;
}

int start_testing()
{
    my_message_t mes;
    FILE *fp = NULL;
    int return_value = 0;
    fp = fopen(FILE_NAME, "a");
    if (!fp)
    {
        return_value = -1;
        goto exit;
    }

    ok(-2 == print_file_mes(fp, &mes, -1), "Тест print_file_mes(fp, &mes, -1), должны получить -2");
    ok(-1 == print_file_mes(fp, NULL, 0), "Тест print_file_mes(fp, NULL, 0), должны получить -1");
    ok(-1 == print_file_mes(NULL, &mes, 0), "Тест print_file_mes(NULL, &mes, 0), должны получить -1");
    ok(-1 == print_file_mes(NULL, NULL, -1), "Тест print_file_mes(NULL, NULL, -1), должны получить -1");

    return_value = remove(FILE_NAME);
    if (return_value < 0)
    {
        return_value  = -1;
        goto exit;
    }

exit:
    return return_value;
}

