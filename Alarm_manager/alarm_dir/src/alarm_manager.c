#include "myDemon.h"
#define TCP_PORT_RECV "tcp://*:3536"    
#define TCP_PORT_MANAGER "tcp://*:5555" 
#define FILE_NAME "alarm.log"
#define ZLOG_FILE_CONF "alarm_manager.conf"
#define ZLOG_CATEGORY "rcv_msg"

static pthread_mutex_t mutex;
static void* context = NULL;
static zlog_category_t *log_mes = NULL;
static unsigned int msize_message = 0;
static bool flag = false, clean_mes = false;

int main(int argc, char *argv[])
{
    int return_status = 0;
    int status[NUM_THREAD] = {0};
    unsigned int i = 0, log_wr = 0;
    pthread_t thread[NUM_THREAD] = {0};
    my_message_t args[N] = {0};
    FILE *fp = NULL;
    
    log_wr = zlog_init(ZLOG_FILE_CONF);

    if (log_wr)
    {
        perror("zlog_init");
        return_status = -1;
        goto exit;
    }

    log_mes = zlog_get_category(ZLOG_CATEGORY);

    if (!log_mes)
    {
        perror("zlog_get_category");
        return_status = -1;
        goto exit;
    }

    context  = zmq_ctx_new();
    if (!context)
    {
        zlog_error(log_mes, "context\n");
        return_status = -1;
        goto exit;
    }

    if ((pthread_mutex_init(&mutex, NULL)) != 0)
    {
        zlog_error(log_mes, "pthread_mutex_init");
        return_status = -1;
        goto exit;
    }

    /* Прием сообщений и запись в очередь */
    
    status[i] = pthread_create(&thread[i], NULL, create_recieve_fun, &args);

    if (status[i] != 0)
    {
        zlog_error(log_mes, "pthread_create");
        return_status = -1;
        goto exit;
    }

    /* Управление очередью, передача сообщений менеджеру */ 
    
    i++;
    status[i] = pthread_create(&thread[i], NULL, create_manager_fun, &args);

    if (status[i] != 0)
    {
        zlog_error(log_mes, "pthread_create");
        return_status = -1;
        goto exit;
    }

    /* Запись в файл */

    while (1)
    {
        pthread_mutex_lock(&mutex);
        if (msize_message < N && flag == true && clean_mes == false)
        {
            if ((fp = fopen(FILE_NAME, "a")) == NULL)
            {
                zlog_error(log_mes, "fopen");
                return_status = -1;
                goto exit;
            }

            return_status = print_file_mes(fp, args, msize_message-1);
            if (return_status < 0)
            {
                zlog_error(log_mes, "fail print_file_mes");
            }

            flag = false;
            fclose(fp);
        }

        else if (msize_message < N && flag == true && clean_mes == true)
        {
            if ((fp = fopen(FILE_NAME, "w")) == NULL)
            {
                zlog_error(log_mes, "fopen");
                return_status = -1;
                goto exit;
            }

            return_status = print_file_mes(fp, args, msize_message-1);
            if (return_status < 0)
            {
                zlog_error(log_mes, "fail print_file_mes");
            }

            flag = false;
            clean_mes = false;
            fclose(fp);
        }

        else if (msize_message == N && flag == true)
        {
            if ((fp = fopen(FILE_NAME, "w")) == NULL)
            {
                zlog_error(log_mes, "fopen");
                return_status = -1;
                goto exit;
            }

            for (int i = 0; i < N; i++)
            {
                return_status = print_file_mes(fp, args, i);
                if (return_status < 0)
                {
                    zlog_error(log_mes, "fail print_file_mes");
                }
            }

            flag = false;
            clean_mes = false;
            fclose(fp);
        }

        pthread_mutex_unlock(&mutex);
        usleep(1000);
    }

exit:
    fclose(fp);
    zlog_fini();
    zmq_ctx_destroy(context);
    pthread_mutex_destroy(&mutex);
    return return_status;
}

int print_file_mes(FILE *fp, my_message_t *arg, int num)
{
    time_t t;
    struct tm *t_m = NULL;
    int return_status = 0;

    if (!fp || !arg)
    {
        zlog_error(log_mes, "incorrect arg in print_file_mes");
        return_status = -1;
        goto exit;
    }

    if (num < 0 || num > N)
    {
        zlog_error(log_mes, "incorrect arg in print_file_mes");
        return_status = -2;
        goto exit;
    }
    
    t = time(NULL);
    t_m = localtime(&t);
    fprintf(fp, "[%d-%d-%d %d:%d:%d] [%d|%d|%d|%s]\n", 1900 + t_m->tm_year, 
    t_m->tm_mon, t_m->tm_mday, t_m->tm_hour, t_m->tm_min, t_m->tm_sec,
    arg[num].pid, arg[num].priority, arg[num].type, arg[num].mes);

    fflush(fp);

exit:
    return return_status;
}

void *create_recieve_fun(void *args)
{
    my_message_t *message = NULL;
    my_message_t buffer_message = {0};
    int rc = 0;
    void* sock_recv = NULL;
    int rcv_byte = 0;
    my_message_t *pointer = NULL;

    if (pthread_detach(pthread_self()) != 0)
    {
        zlog_error(log_mes, "fail pthread_detach");
        goto exit;
    }

    if (!args)
    {
        zlog_error(log_mes, "fail argument in create_recieve_fun\n");
        goto exit;
    }

    pointer = (my_message_t*)malloc(sizeof(my_message_t));
    if (!pointer)
    {
        zlog_error(log_mes, "malloc");
        goto exit;
    }

    message = (my_message_t*)args;
    pointer = &message[0];

    sock_recv = zmq_socket(context, ZMQ_PULL);
    if (!sock_recv)
    {
        zlog_error(log_mes, "zmq_socket");
        goto exit;
    }

    rc = zmq_bind(sock_recv, TCP_PORT_RECV);
    if (rc != 0)
    {
        zlog_error(log_mes, "zmq_bind");
        goto exit;
    }

    while (1)
    {
        rcv_byte = zmq_recv (sock_recv, &buffer_message, sizeof(my_message_t), 0);
        if (rcv_byte < 0)
        {
            zlog_error(log_mes, "zmq_recv");
        }

        pthread_mutex_lock(&mutex);

        if (msize_message >= N)
        {
            zlog_warn(log_mes, "[%d|%d|%d|%s]", pointer->pid,
            pointer->priority, pointer->type, pointer->mes);

            memcpy(pointer, &buffer_message, sizeof(my_message_t));
          
            zlog_info(log_mes, "[%d|%d|%d|%s]", pointer->pid,
            pointer->priority, pointer->type, pointer->mes);

            if (pointer == &message[N-1])
            {
                pointer = &message[0];
            }
            else
            {
                pointer++;
            }

            flag = true;
            clean_mes = true;
        }
        else if (msize_message < N)
        {
            memcpy(&message[msize_message], &buffer_message, sizeof(my_message_t));
            memset(&buffer_message, 0, sizeof(my_message_t));

            zlog_info(log_mes, "[%d|%d|%d|%s]", message[msize_message].pid,
            message[msize_message].priority, message[msize_message].type,
            message[msize_message].mes);

            flag = true;
            msize_message++;
        }

        pthread_mutex_unlock(&mutex);
        usleep(1000);
    }

exit:
    free(pointer);
    zmq_close(sock_recv);
    zmq_ctx_destroy(context);
    return NULL;
}

void *create_manager_fun(void *args)
{
    int *num = NULL;
    int rc = 0;
    void *responder = NULL;
    my_message_t *message = NULL;
    int rcv_byte = 0, send_byte = 0;
    int *buffer = NULL;

    if (pthread_detach(pthread_self()) != 0)
    {
        zlog_error(log_mes, "fail pthread_detach");
        goto exit;
    }

    if (!args)
    {
        zlog_error(log_mes, "fail argument in create_manager_fun\n");
        goto exit;
    }

    message = (my_message_t*)args;
    responder = zmq_socket (context, ZMQ_REP);
    if (!responder)
    {
        zlog_error(log_mes, "zmq_socket");
        goto exit;
    }

    rc = zmq_bind(responder, TCP_PORT_MANAGER);
    if (rc != 0)
    {
        zlog_error(log_mes, "zmq_bind");
        goto exit;
    }

    buffer = (int*)malloc(sizeof(int));
    if (!buffer)
    {
        zlog_error(log_mes, "malloc");
        goto exit;
    }

    num = (int*)malloc(sizeof(int));
    if (!num)
    {
        zlog_error(log_mes, "malloc");
        goto exit;
    }

    while (1)
    {
        rcv_byte = zmq_recv (responder, buffer, sizeof(*buffer), 0);
        if (rcv_byte < 0)
        {
            zlog_error(log_mes, "zmq_recv");
        }

        if (*buffer == DELETE_ALL)
        {
            pthread_mutex_lock(&mutex);

            for (int i = 0; i < N; i++)
            {
                memset(&message[i], 0, sizeof(my_message_t));
            }
            
            zlog_info(log_mes, "Delete all message");
            msize_message = 0;
            flag = false;
            clean_mes = true;
            pthread_mutex_unlock(&mutex);

            *num = ACK;
            send_byte = zmq_send(responder, num, sizeof(*num), 0);
            if (send_byte < 0)
            {
                zlog_error(log_mes, "zmq_send");
            }
        }

        else if (*buffer == READ_ALL)
        {
            pthread_mutex_lock(&mutex);
            /*Проверим размер очереди*/

            if (msize_message == 1)
            {
                send_byte = zmq_send(responder, &message[0], sizeof(my_message_t), 0);
                if (send_byte < 0)
                {
                    zlog_error(log_mes, "zmq_send");
                }
            }

            else if (msize_message > 1)
            {
                for (int i = 0; i < msize_message -1 ; i++)
                {
                    send_byte = zmq_send(responder, &message[i], sizeof(my_message_t), ZMQ_SNDMORE);
                    if(send_byte < 0)
                    {
                        zlog_error(log_mes, "zmq_send");
                    }
                }

                send_byte = zmq_send(responder, &message[msize_message-1], sizeof(my_message_t), 0);
                if(send_byte < 0)
                {
                    zlog_error(log_mes, "zmq_send");
                }
            }

            else if (msize_message == 0)
            {
                send_byte = zmq_send(responder, NULL, 0, 0);
                if (send_byte < 0)
                {
                    zlog_error(log_mes, "zmq_send");
                }
            }
             
            pthread_mutex_unlock(&mutex);
        }

        memset(buffer, 0, sizeof(*buffer));
        usleep(1000);
    }

exit:
    free(num);
    free(buffer);
    zmq_close(responder);
    zmq_ctx_destroy(context);
    return NULL;
}
