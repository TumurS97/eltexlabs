# Alarm manager
Программа предоставляет сервис для приема и хранения N сообщений от неограниченного количества процессов, а также обеспечивает синхронизированную запись пришедших сообщений на диск, с целью их просмотра в случае перезагрузки устройства.

Основной поток - **Thread main**, порождает вспомогательный поток для приема и записи сообщений в очередь от множества процессов - **Thread recieve** и поток, необходимый для связи с управляющий клиентом - **Thread manager**. Кроме того, **Thread main** обеспечивает синхронизированную запись собщений на диск.

В программе используются cледующие вспомогательные переменные:
```c
static bool flag = false; /*Используется для проверки наличия сообщений в очереди true - сообщения есть*/
static bool clean_mes = false; /*Используется для очищения файла, в который производиться запись сообщений true -очистить */
static unsigned int msize_message = 0; /*Используется для подсчета количества сообщений*/
```
## Pthread reciever 
Приложение создает новый поток в котором открывает ZMQ сокет, на который могут слать сообщения, неограниченное количество других процессов.
```c
int main(int argc, char *argv[])
{
    ...
    status[i] = pthread_create(&thread[i], NULL, create_recieve_fun, &args);
    ...
}
```  
Каждое пришедшее сообщение, сначала записывается в приёмный буффер, а затем копируется в кольцевой буфер, запись в который защищена мьютексом, кроме того, все операции с флагами и очередью сообщений, а также запись в файл защищены мьютексом. В кольцевом буфере (очереди) хранятся последние N сообщений.
```c
void *create_recieve_fun(void *args)
{   ...
    while(1)
    {
        ...
        rcv_byte = zmq_recv (sock_recv, &buffer_message, sizeof(my_message_t), 0);
        ...
        pthread_mutex_lock(&mutex);
        ...
        memcpy(pointer, &buffer_message, sizeof(my_message_t));
        ...
        pthread_mutex_unlock(&mutex);
        ...
    }
    ...
}
```
Кольцевой буфер реализован с помощью **my_message_t * pointer**, который указывает по какому индексу производить запись сообщения в буффер.

После приёма сообщения в приёмный буфер, проверяется состояние очереди. Если очередь свободна, то сообщение просто копируется в кольцевой буфер по индексу **msize_message**, при этом **flag = true**. Если очередь заполнена, то сообщение копируется в кольцевой буфер по индексу, на который указывает **pointer**, после чего **pointer++** инкрементируется, при этом **flag = true, clean_mes = true**, то есть содержимое файла будет перезаписано.

```c
void *create_recieve_fun(void *args)
{
    ...
    while(1)
    {
        rcv_byte = zmq_recv (sock_recv, &buffer_message, sizeof(my_message_t), 0);
        ...
        if (msize_message >= N)
        {
            memcpy(pointer, &buffer_message, sizeof(my_message_t));
        
            if (pointer == &message[N-1])
            {
                pointer = &message[0];
            }
            else
            {
                pointer++;
            }

            flag = true;
            clean_mes = true;
        }
        else if (msize_message < N)
        {
            memcpy(&message[msize_message], &buffer_message, sizeof(my_message_t));
            flag = true;
            msize_message++;
        }
        ...
    }
    ...
}
```
## Pthread manager

Приложение создаёт новый поток и открывает в нем ZMQ сокет, для общения с клиентом, чтобы принимать управляющие сообщения.
```c
int main(int argc, char *argv[])
{
    ...
    status[i] = pthread_create(&thread[i], NULL, create_manager_fun, &args);
    ...
}
``` 
Данный поток принимает управляющие сообщения от клиента в буффер, а затем сравнивает значени флага в сообщении с константой. Если выставлен флаг *DELETE_ALL*, то кольцевой буфер полностью очищается, а клиенту отправляется сообщение - подтверждение о успешном удалении всех сообщений. При этом выставляются значения:
```c
msize_message = 0;
flag = false;
clean_mes = true;
```
Если выставлен флаг *READ_ALL*, и очередь не пуста, то, в зависимости от размера очереди, все сообщения отправляются клиенту. Если очередь пуста, то клиенту отправляется нулевое сообщение.
```c
void *create_manager_fun(void *args)
{
	...
    while (1)
    {
        rcv_byte = zmq_recv (responder, buffer, sizeof(*buffer), 0);
        if (*buffer == DELETE_ALL)
        {
            ...
        }
        else if (*buffer == READ_ALL)
        {
            if (msize_message == 1)
            {
                ...
            }

            else if (msize_message > 1)
            {
                ...
            }

            else if (msize_message == 0)
            {
                ...
            }
        }
        ...
    }
    ...
}
```
## Pthread main
Основной поток, запускает дополнительные потоки **Pthread reciever** и **Pthread manager**, а также в нем выполняется запись сообщений в файл и синхронизация файла и очереди сообщений.

Если пришли новые сообщение и в очереди есть место, то файл открывается просто для дозаписи:
```c
int main(int argc, char *argv[])
{
    if (msize_message < N && flag == true && clean_mes == false)
    {
        if ((fp = fopen(FILE_NAME, "a")) == NULL)
        {
            ...
        }
        ...
    }
}
```
Если пришли новые сообщения, а очередь заполнена, или было получено сообщение от клиента, о том, что нужно очистить очередь, то файл открывается для перезаписи:
```c
else if (msize_message < N && flag == true && clean_mes == true)
{
    if ((fp = fopen(FILE_NAME, "w")) == NULL)
    {
        ...
    }
    ...
}
else if (msize_message == N && flag == true)
{
    if ((fp = fopen(FILE_NAME, "w")) == NULL)
    {
        ...
    }
    ...
}

```


