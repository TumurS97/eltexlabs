# Alarm manager
Разрабокта сервиса (демона) для приема и хранения N сообщений от неограниченного количества клиентов, а также библиотеки для конфигурирования и управления этим сервисом. 

## Get start
```
git clone http://gitlab.eltex.loc/timur.sajbotalov/Alarm_manager.git
```
## Prerequisites
Перед сборкой программы необходимо установить библиотеки zlog, для логирования сообщений и ZMQ, для использования в качестве IPC. 

Сборка и установка zlog:
```
Download:https://github.com/HardySimpson/zlog/archive/latest-stable.tar.gz
```
```
$ tar -zxvf zlog-latest-stable.tar.gz
$ cd zlog-latest-stable/
$ make
$ sudo make install
or
$ sudo make PREFIX=/usr/local/ install 
```
PREFIX указывает, где установлен zlog. После установки измените системные настройки, чтобы ваша программа могла найти библиотеку zlog
```
$ sudo cat /etc/ld.so.conf
/usr/local/lib
$ sudo ldconfig 
```
Сборка и установка zlog:
```
$ echo "deb http://download.opensuse.org/repositories/ 
       network:/messaging:/zeromq:/release-stable/Debian_9.0/ ./" >> /etc/apt/sources.list
$ wget https://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/Debian_9.0/Release.key -O- | sudo apt-key add
$ apt-get install libzmq3-dev
```