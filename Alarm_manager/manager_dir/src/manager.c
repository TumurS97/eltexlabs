#include "myDemon.h"
#define TCP_PORT  "tcp://localhost:5555"
#define ZLOG_FILE_CONF "manager.conf"
#define ZLOG_CATEGORY "manager_msg"

static void *context = NULL;
static zlog_category_t *log_mes = NULL;

int main (int argc, char *argv[])
{
    int fild = 0, input = 0, value = 0; 
    int return_value = 0;
    srand(getpid());

    return_value = create_zlog();
    if (return_value < 0)
    {
        perror("create_zlog()");
        return_value = -1;
        goto exit;
    }

    context = zmq_ctx_new ();
    if (!context)
    {
        zlog_error(log_mes,"context");
        return_value = -1;
        goto exit;
    }

    return_value = create_connections(context, log_mes);
    if (return_value < 0)
    {
        zlog_error(log_mes, "create_connections");
        return_value = -1;
        goto exit;
    }

    return_value = check_the_args(argc , argv);
    if (return_value < 0)
    {
        zlog_error(log_mes, "check_the_args");
        return_value = -1;
        goto exit;
    }

    zlog_notice(log_mes, "Action: Delete all[1] Read all[2] Read by filtr[3] Send message[4]\nEnter:");

    scanf( "%d", &input );
    if (input == FILTR)
    {
        zlog_notice(log_mes, "Filtr by fild: module[1] priority[2] active alarm[3]\nEnter: ");
        scanf("%d", &fild);
        if (fild != FILTR_MODULE && fild != FILTR_PRIORITY && fild != FILTR_ACTIVE)
        {
            zlog_notice(log_mes, "Incorrect input");
            return_value = -1;
            goto exit;
        }
        if (fild == FILTR_PRIORITY ||  fild == FILTR_MODULE)
        {
            zlog_notice(log_mes, "Enter value:");
            scanf("%d", &value);
        }
    }

    switch (input) 
    {
        case DELETE_ALL:  /*Удаляет все сообщения из очереди */

            return_value = delete_all();
            if (return_value < 0)
            {
                zlog_error(log_mes, "delete fail");
                return_value = -1;
                goto exit;
            }
            break;
        
        case READ_ALL: /*Вычитывает все сообщения*/
    
            return_value = write_all();
            if (return_value < 0)
            {
                zlog_error(log_mes, "write_all fail");
                return_value = -1;
                goto exit;
            }

            break;

        case FILTR: /*Вычитывает все сообщение по соответствующему фильтру */

            return_value = write_by_filt(fild, value);
            if (write_by_filt < 0)
            {
                zlog_error(log_mes, "write_by_filt fail");
                return_value = -1;
                goto  exit;
            }
            
            break;

        case SEND_MES:

            return_value = send_message(argv[1]);
            if (return_value < 0)
            {
                zlog_notice(log_mes, "send_message");
                return_value = -1;
                goto exit;
            }
            
            break;


        default:
            zlog_error(log_mes, "incorrect input\n");
            return_value = -1;
            goto exit;
    }

exit:
    close_connections();
    zmq_ctx_destroy (context);
    return return_value;
}

int check_the_args(int argc, char **argv)
{
    int return_value = 0;
    if (argc != 2)
    {
        zlog_error(log_mes, "Usage: ./manager <message>");
        return_value = -1;
        goto exit;
    }
    if (!argv)
    {
        zlog_error(log_mes, "fail arg, usage: ./manager <messgae>");
        return_value = -2;
        goto exit;
    }
exit:
    return return_value;
}

int create_zlog()
{
    int return_value = 0;
    unsigned int log_wr = 0;

    log_wr = zlog_init(ZLOG_FILE_CONF);

    if (log_wr)
    {
        perror("zlog_init");
        return_value = -1;
        goto exit;
    }

    log_mes = zlog_get_category(ZLOG_CATEGORY);

    if (!log_mes)
    {
        perror("zlog_get_category");
        return_value = -1;
        goto exit;
    }

exit:
    return return_value;
}