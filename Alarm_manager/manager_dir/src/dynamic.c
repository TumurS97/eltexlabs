#include "myDemon.h"
#define TCP_PORT  "tcp://localhost:5555"
#define PORT_MANAGER "tcp://localhost:3536"

static void *reciever = NULL;
static void *requester = NULL;
static zlog_category_t *log_mes = NULL;

int delete_all()
{
    int *buffer = NULL, *num = NULL;
    int byte_rcv = 0, byte_send = 0;
    int return_status = 0;

    if (!requester)
    {
        zlog_error(log_mes, "fail argument in delete");
        return_status = -1;
        goto exit;
    }

    buffer = (int*)malloc(sizeof(int));
    if (!buffer)
    {
        zlog_error(log_mes, "malloc");
        return_status = -1;
        goto exit;
    }

    num = (int*)malloc(sizeof(int));
    if (!num)
    {
        zlog_error(log_mes, "malloc");
        return_status = -1;
        goto exit;
    }

    *num = DELETE_ALL;

    byte_send = zmq_send(requester, num, sizeof(num), 0);
    if (byte_send < 0)
    {
        zlog_error(log_mes, "zmq_send");
        return_status = -2;
        goto exit;
    }

    byte_rcv = zmq_recv(requester, buffer, sizeof(buffer), 0);
    if (byte_rcv < 0)
    {
        zlog_error(log_mes, "zmq_send");
        return_status = -3;
        goto exit;
    }

    if (*buffer == ACK)
    {
        zlog_notice(log_mes, "Delete success \n");
    }
    else
    {
        return_status = -10;
        goto exit;
    }

exit:
    free(num);
    free(buffer);
    
    return return_status;
}

int write_all()
{
    int size_mes = 0;
    my_message_t args[N] = {0};
    int return_status = 0;

    if (!requester)
    {
        zlog_error(log_mes, "fail argument in delete");
        return_status = -1;
        goto exit;
    }

    size_mes = rcv_mesage(args);
    if (size_mes < 0)
    {
        zlog_error(log_mes, "rcv_mesage");
        return_status = -1;
        goto exit;
    }

    if (size_mes == 1)
    {
        if (args[0].pid == 0)
        {
            zlog_notice(log_mes, "Empty queue\n");
            goto exit;
        }
    }

    for (unsigned int i = 0; i < size_mes; i++)
    {
        print_message(args, i);
    }

exit:
    return return_status;
};

int write_by_filt(filtr_parametrs_e fild, int value)
{
    my_message_t args[N] = {0};
    int size_mes = 0;
    int return_status = 0;

    if (!requester)
    {
        zlog_error(log_mes, "fail argument in delete");
        return_status = -1;
        goto exit;
    }

    size_mes = rcv_mesage(args);
    if (size_mes < 0)
    {
        zlog_error(log_mes, "rcv_mesage");
        return_status = -1;
        goto exit;
    }


    if (fild == FILTR_MODULE)
    {
        for (unsigned int i = 0; i < size_mes; i++)
        {
            if (args[i].pid == value)
            {
                print_message(args, i);
            }
        }      
    }

    else if (fild == FILTR_PRIORITY)
    {
        for (unsigned int i = 0; i < size_mes; i++)
        {
            if (args[i].priority == value)
            {
                print_message(args, i);
            }
        }
    }

    else if (fild == FILTR_ACTIVE)
    {
        search_active_alarm(args, size_mes);
    }

    else
    {
        zlog_error(log_mes, "fail arg");
        return_status = -1;
    }

exit:
    return return_status;
}

int search_active_alarm(my_message_t *args, int size)
{
    int return_status = 0;

    if (!args)
    {
        zlog_error(log_mes, "fail argument in search_active_alarm");
        return_status = -1;
        goto exit;
    }

    if (size < 0 || size < N)
    {
        zlog_error(log_mes, "fail argument in search_active_alarm");
        return_status = -2;
        goto exit;
    }

    for (int i = 0; i < size; i++)
    {
        if (args[i].type == ALARM)
        {
            for (int j = 0; j < size; j++)
            {
                if ( i != j 
                    && args[i].pid == args[j].pid  
                    && args[j].type != NORMILIZE 
                    && strcmp(args[i].mes, args[j].mes) == 0)
                {
                    print_message(args, i);
                    break;
                }
            }
        }
    }

exit:
    return return_status;
}

int rcv_mesage(my_message_t *arg)
{
    int *num = NULL;
    int64_t more = 0;
    size_t more_size = 0;
    unsigned int i = 0;
    int byte_send = 0, byte_rcv = 0;
    int return_status = 0;

    if(!requester || !arg)
    {
        zlog_error(log_mes, "fail arg in rcv_mesage");
        return_status = -1;
        goto exit;
    }

    num = (int*)malloc(sizeof(int));
    if (!num)
    {
        zlog_error(log_mes, "malloc");
        return_status = -1;
        goto exit;
    }

    *num = READ_ALL;

    byte_send = zmq_send (requester, num, sizeof(*num), 0);
    if(byte_send < 0)
    {
        zlog_error(log_mes, "zmq_send");
        return_status = -2;
        goto exit;
    }

    more_size = sizeof(more);
    do
    {
        byte_rcv = zmq_recv(requester,&arg[i], sizeof(my_message_t), 0);
        if (byte_rcv < 0)
        {
           zlog_error(log_mes, "zmq_recv");
           return_status = -3;
           goto exit;
        }

        i++;

        if ((zmq_getsockopt(requester, ZMQ_RCVMORE, &more, &more_size)) < 0)
        {
            zlog_error(log_mes, "zmq_getsockopt");
            return_status = -4;
            goto exit;
        }
        
    } while (more);

    return_status = i;

exit:
    free(num);
    return return_status;
}

inline void print_message(my_message_t *args, unsigned int  i)
{
    if (!args)
    {
        zlog_error(log_mes,"fail arg in print_message");
    }
    else
    {
        zlog_info(log_mes,"[%d|%d|%d|%s]", args[i].pid, args[i].priority,
        args[i].type, args[i].mes );
        fflush(stdout);
    }
}

int create_connections(void *context, zlog_category_t *logger)
{
    int return_value = 0;
    int conn = 0;

    log_mes = logger;
    if (!log_mes)
    {
        perror("zlog_category_t");
        return_value = -1;
        goto exit;
    }

    if (!context)
    {
        zlog_error(log_mes, "context");
        return_value = -1;
        goto exit;
    }

    reciever = zmq_socket(context, ZMQ_PUSH);
    if(!reciever)
    {
        zlog_error(log_mes, "zmq_socket");
        return_value = -1;
        goto exit;
    }

    requester = zmq_socket(context, ZMQ_REQ);
    if(!requester)
    {
        zlog_error(log_mes, "requester");
        return_value = -1;
        goto exit;
    }

    conn = zmq_connect(reciever, PORT_MANAGER);
    if (conn < 0 )
    {
        zlog_error(log_mes, "zmq_connect");
        return_value = -1;
        goto exit;
    }

    conn = zmq_connect(requester, TCP_PORT);
    if (conn < 0 )
    {
        zlog_error(log_mes, "zmq_connect");
        return_value = -1;
        goto exit;
    }

exit:
    return return_value;
}

int create_message(my_message_t *arg, char *argv)
{
    int return_status = 0;
    if (!arg || !argv)
    {
        zlog_error(log_mes, "fail arg in create_message");
        return_status = -1;
        goto exit;
    }

    return_status = snprintf(arg->mes, LEN, "%s", argv);
    if (return_status < 0)
    {
        zlog_error(log_mes, "snprintf");
        return_status = -1;
        goto exit;
    }

    arg->priority = rand() % 20 + 1;
    arg->type = ALARM;
    arg->pid = getpid();

    if (arg->pid <= 0)
    {
        zlog_error(log_mes, "create_message");
        return_status = -1;
        goto exit;
    }

exit:
    return return_status;
}

int send_message(char *argv)
{
    my_message_t *arg = NULL;
    int return_status = 0;
    int send_byte = 0;

    if(!reciever || !argv)
    {
        zlog_error(log_mes, "fail args in send_message_to_demon");
        return_status = -1;
        goto exit;
    }

    arg = (my_message_t*)malloc(sizeof(my_message_t));
    if(!arg)
    {
        zlog_error(log_mes, "malloc");
        goto exit;
    }

    return_status = create_message(arg, argv);
    if (return_status < 0)
    {
        return_status = -2;
        goto exit;
    }

    zlog_info(log_mes, "Send: [%d|%d|%d|%s]", arg->pid,
    arg->priority, arg->type, arg->mes);

    send_byte = zmq_send(reciever, arg, sizeof(*arg), 0);
    if (send_byte < 0)
    {
        zlog_error(log_mes, "zmq_send");
        return_status = -2;
        goto exit;
    }

exit:
    free(arg);
    
    return return_status;
}

int close_connections()
{
    int return_value = 0;
    return_value  = zmq_close(reciever);
    if (return_value < 0)
    {
        zlog_error(log_mes, "zmq_close");
        return_value  = -1;
        goto exit;
    }

    return_value  = zmq_close(requester);
    if (return_value < 0)
    {
        zlog_error(log_mes, "zmq_close");
        return_value  = -1;
        goto exit;
    }

exit:
    return return_value;
}


