#include <tap.h>
#include <fff.h>
#include "myDemon.h"
#define ZLOG_FILE_CONF "test.conf"
#define ZLOG_CATEGORY "test_msg"

int create_test_connect();
int start_test();
int my_custom_value_fake1(void *requester, void *buffer, size_t num, int value);
int my_custom_value_fake2(void *requester, void *buffer, size_t num, int value);


static void *context = NULL;
static zlog_category_t *log_mes = NULL;

DEFINE_FFF_GLOBALS;
FAKE_VALUE_FUNC(int, zmq_send, void*, const void*, size_t, int);
FAKE_VALUE_FUNC(int, zmq_recv, void*, void*, size_t, int);
FAKE_VALUE_FUNC(int, zmq_getsockopt, void*, int, void*, size_t*);

int main () 
{
    int return_status = 0;
    unsigned int log_wr = 0;

    log_wr = zlog_init(ZLOG_FILE_CONF);

    if (log_wr)
    {
        perror("zlog_init");
        return_status = -1;
        goto exit;
    }

    log_mes = zlog_get_category(ZLOG_CATEGORY);

    if (!log_mes)
    {
        return_status = -1;
        goto exit;
    }

    context = zmq_ctx_new ();
    if (!context)
    {
        return_status = -1;
        goto exit;
    }

    return_status = create_connections(context, log_mes);
    if (return_status < 0)
    {
        return_status = -1;
        goto exit;
    }
    start_test();
exit:
    return 0;
}

int start_test()
{
    my_message_t arg={0};
    char mas[N];

    plan(19);
    ok(0 == delete_all(), "Тест delete_all(), должны получить 0");
    zmq_send_fake.return_val = -1;
    ok(-2 == delete_all(), "Тест delete_all(), должны получить -2");
    RESET_FAKE(zmq_send);
    zmq_recv_fake.return_val = -1;
    ok(-3 == delete_all(), "Тест delete_all(), должны получить -3");
 
    RESET_FAKE(zmq_send);
    RESET_FAKE(zmq_recv);
    RESET_FAKE(zmq_getsockopt);
    ok(1 == rcv_mesage(&arg), "Тест rcv_mesage(&arg), должны получить 1");
    ok(-1 == rcv_mesage(NULL), "Тест rcv_mesage(NULL), должны получить -1");
    zmq_send_fake.return_val = -1;
    ok(-2 == rcv_mesage(&arg), "Тест rcv_mesage(&arg), должны получить -2");
    RESET_FAKE(zmq_send);
    zmq_recv_fake.return_val = -1;
    ok(-3 == rcv_mesage(&arg), "Тест rcv_mesage(&arg), должны получить -3");
    RESET_FAKE(zmq_send);
    RESET_FAKE(zmq_recv);
    zmq_getsockopt_fake.return_val = -1;
    ok(-4 == rcv_mesage(&arg), "Тест rcv_mesage(&arg), должны получить -4");

    ok(-1 == write_by_filt(0,0), "Тест write_by_filt(0,0), должны получить -1");
    ok(-1 == write_by_filt(-1,-1), "Тест write_by_filt(-1,-1), должны получить -1");

    ok(-1 == search_active_alarm(NULL,0), "Тест search_active_alarm(NULL,0), должны получить -1");
    ok(-2 == search_active_alarm(&arg,-1), "Тест search_active_alarm(&arg,-1), должны получить -2");

    ok(-1 == create_message(&arg,NULL), "Тест create_message(&arg,NULL), должны получить -1");
    ok(-1 == create_message(NULL,NULL), "Тест create_message(NULL,NULL), должны получить -1");
        
    ok(-1 == send_message(NULL), "Тест send_message(NULL), должны получить -1");
    ok(0 == send_message(mas), "Тест send_message(&arg), должны получить 0");
    zmq_send_fake.return_val = -1;
    ok(-2 == send_message(mas), "Тест send_message(&arg), должны получить -2");

    RESET_FAKE(zmq_send);
    RESET_FAKE(zmq_recv);
    RESET_FAKE(zmq_getsockopt);

    zmq_recv_fake.custom_fake = my_custom_value_fake1;
    ok(-10 == delete_all(), "Тест delete_all() на некорректный ACK, должны получить -10");
    RESET_FAKE(zmq_recv);
    zmq_recv_fake.custom_fake = my_custom_value_fake2;
    ok(0 == delete_all(), "Тест delete_all() на корректный ACK, должны получить 0");

    done_testing();
    return 0;
}

int my_custom_value_fake1(void *requester, void *buffer, size_t num, int value)
{
    *(int*)buffer = -125;
    return 0;
}

int my_custom_value_fake2(void *requester, void *buffer, size_t num, int value)
{
    *(int*)buffer = 10;
    return 0;
}

