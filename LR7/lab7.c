#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
//Поиск всех простых чисел

int *searchNums(int *mas,int number, int del, int i,int key)
{
	sleep(i);
	int *buffer=malloc(sizeof(int)*number/del);
	int n=0;
	int over=0;
	if(key==1)
	over=number;
	else
	over=i*(number/del)+(number/del);
	int k=0;
	for(int j=i*(number/del); j<over;j++)
	{
		for(int t=0; t< number;t++)
			{
				if (mas[t]>mas[j])
				break;
				
				if(mas[j]%mas[t]==0)
				k=k+1;
				
			}
			if (k==2)
			{
			//printf("you %d\n",mas[j]);
			buffer[n]=mas[j];
			n++;
			}
			k=0;	
	}

	return buffer;
}

int main(int argc, char *argv[])
{
	if(argc<3)
	{
		printf("Введите максимальное число и количество диапазонов ./true.c <число>  <кол-во диапазонов>\n");
		exit(1);
	}
	int number=0,del=0;
	number=atoi(argv[1]);
	del=atoi(argv[2]);
	
	//printf("num=%d\ndel=%d\n",number,del);
	int mas[number];
	int key=0;
	for(int i=0;i<number;i++)
	mas[i]=i+1;
	
	int status=0,stat=0;
	int fd[del][2];
	int pid[del];
	for (int i = 0; i < del; i++)
	 {
		pipe(fd[i]);
        pid[i] = fork();
        srand(getpid());
	    if (pid[i]==-1) 
		{
            perror("fork"); 
            exit(1); 
        }
        else if (pid[i]==0)
        {
			close(fd[i][0]);
			int *result=NULL;  // сколько и с какого
			int scet=0;
			if(i==del-1)
			key=1;
			result=searchNums(mas,number,del,i,key);
			
			for(int k=0;k<number/del;k++)
			{
				if (result[k]!=0)
				scet=scet+1;	
			}
			write(fd[i][1],&scet,sizeof(int));
			
			for(int k=0;k<scet;k++)
			{
				write(fd[i][1],&result[k],sizeof(int));
			}
					
			exit(0);
        }
        
	}
	
	for(int i=0;i<del;i++)
	{
		status=waitpid(pid[i],&stat,0);
		if(pid[i] == status)
			{
				printf("процесс-потомок %d done:\n", i);
				close(fd[i][1]);
				int name=0;
				read(fd[i][0],&name,sizeof(int));
				printf("size=%d\n",name);
				int qwer[name];
				for(int j=0;j<name;j++)
				{
					read(fd[i][0],&qwer[j],sizeof(int));
					printf("result=%d\n",qwer[j]);
				}	
			}		
	}
}
