#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 124
/*Исключить строки содержащие заданный символ 8 variant*/
void WriteToFile(FILE *fw, char mas[],char *c)
{
	int num=0;
	num=strlen(mas);
	if(!ferror(fw))
	{
		for(int i=0;  i<num;i++)
		{
			if(mas[i]==*c)
			return;
		}
		fprintf(fw,"%s",mas);
	}
}

void TypeToFile(char *str,char *buffer)
{
	int i=0;
	
	char *mas=malloc(sizeof(char)*strlen(str));
	while (str[i] !='\0')
	{
		if(str[i]=='.')
		{
			mas[i]=str[i];
			i++;
			break;
		}
		mas[i]=str[i];
		i++;
	}
	mas[i]='c';
	strcpy(buffer,mas);
	free (mas);	
}

void ReadString(FILE *fp,char *argv[])
{
	char *buffer=malloc(sizeof(char)*MAX_LEN);
	FILE *fw;
	
	TypeToFile(argv[1],buffer);
	
	if((fw=fopen(buffer,"w"))==NULL)
	{
		printf("Не удалось открыть файл в который пишем");
		exit(1);
	}
	if(!ferror(fp))
	{
		while(1)
			{
				fgets(buffer,50,fp);
				if(feof(fp))
				break;
				WriteToFile(fw,buffer,argv[2]);
			}
    }
    if(fclose(fw))
    { 
		printf("Ошибка при закрытии файла.\n");
		exit(1);
	}
	free(buffer);
}

int main (int argc, char *argv[])
{
	FILE *fp;
	if (argc<3)
	{
		printf("Некоректный ввод");
		exit(1);
	}
	
	if((fp=fopen(argv[1], "r"))==NULL)
	{
		printf("Не удалось открыть файл.\n");
		exit(1);
	}
	
	if(strlen(argv[2])>1)
	{
		printf("Некоректный ввод символа удаления! Используйте <Имя файла> <символ>\n");
		exit(1);
	}
	
	ReadString(fp,argv);
	if(fclose(fp))
	{ 
		printf("Ошибка при закрытии файла.\n");
		exit(1);
	}
	printf("Запись в файл <test.c> выполнена успешно.\n");
}

