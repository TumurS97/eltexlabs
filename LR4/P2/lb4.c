#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LEN 124
void WriteToFile(FILE *fw, char c)
{
	if( !ferror(fw))
	fprintf(fw,"%c",c);
}

void TypeToFile(char *str,char *buffer)
{
	int i=0;
	
	char *mas=malloc(sizeof(char)*strlen(str));
	while (str[i] !='\0')
	{
		if(str[i]=='.')
		{
			mas[i]=str[i];
			i++;
			break;
		}
		mas[i]=str[i];
		i++;
	}
	mas[i]='c';
	strcpy(buffer,mas);
	free (mas);	
}

void ReadText(FILE *fp,char *argv[])
{
	char *buffer=malloc(sizeof(char)*MAX_LEN);
	FILE *fw;
	TypeToFile(argv[1],buffer);
	if((fw=fopen(buffer,"w"))==NULL)
	{
		printf("Не удалось открыть файл в который пишем");
		exit(1);
	}
	
	char c;
	if(!ferror(fp))
	{
		while(1)
			{
				c=fgetc(fp);
				if(feof(fp))
				break;
				if(c == argv[2][0])
				c=' ';
				WriteToFile(fw,c);
			}
    }
    if(fclose(fw))
    { 
		printf("Ошибка при закрытии файла.\n");
		exit(1);
	}
	free(buffer);
}

int main(int argc, char *argv[])
{
	FILE *fp;
	
	if(argc<3)
	{
		printf("Некоректный ввод!\n");
		exit(1);
	}
	
	if ((fp=fopen(argv[1],"r"))==NULL)
	{
		printf("Не удалолось открыть файл!\n");
		exit(1);
	}
	
	if(strlen(argv[2])>1)
	{
		printf("Некоректный ввод символа удаления! Используйте <Имя файла> <символ>\n");
		exit(1);
	}
	
	ReadText(fp, argv);
	
	if(fclose(fp))
	{ 
		printf("Ошибка при закрытии файла.\n");
		exit(1);
	}
	printf("Запись в файл выполнена успешно.\n");
}
