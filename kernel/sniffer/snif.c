/*kernel ver. 4.15.47 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/skbuff.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/inet.h>

MODULE_AUTHOR("Nick Chi");
MODULE_DESCRIPTION("Basic netfilter module");
MODULE_LICENSE("GPL");
 
static struct nf_hook_ops nfin;
static char *str_param;
static int number;
static unsigned long cancel[10];
static int i=0;
module_param(number, int, 0755);
module_param(str_param, charp, 0755);

void ip_hl_to_str(unsigned int ip, char *ip_str) 
{
    /*convert hl to byte array first*/
    unsigned char ip_array[4];
    memset(ip_array, 0, 4);
    ip_array[0] = (ip_array[0] | (ip >> 24));
    ip_array[1] = (ip_array[1] | (ip >> 16));
    ip_array[2] = (ip_array[2] | (ip >> 8));
    ip_array[3] = (ip_array[3] | ip);
    sprintf(ip_str, "%u.%u.%u.%u", ip_array[0], ip_array[1], ip_array[2], ip_array[3]);
}

unsigned int ip_str_to_hl(char *ip_str) 
{
    /*convert the string to byte array first, e.g.: from "131.132.162.25" to [131][132][162][25]*/
    unsigned char ip_array[4];
    int i = 0;
    unsigned int ip = 0;
    if (ip_str==NULL) {
        return 0;
    }
    memset(ip_array, 0, 4);
    while (ip_str[i]!='.') {
        ip_array[0] = ip_array[0]*10 + (ip_str[i++]-'0');
    }
    ++i;
    while (ip_str[i]!='.') {
        ip_array[1] = ip_array[1]*10 + (ip_str[i++]-'0');
    }
    ++i;
    while (ip_str[i]!='.') {
        ip_array[2] = ip_array[2]*10 + (ip_str[i++]-'0');
    }
    ++i;
    while (ip_str[i]!='\0') {
        ip_array[3] = ip_array[3]*10 + (ip_str[i++]-'0');
    }
    /*convert from byte array to host long integer format*/
    ip = (ip_array[0] << 24);
    ip = (ip | (ip_array[1] << 16));
    ip = (ip | (ip_array[2] << 8));
    ip = (ip | ip_array[3]);
    //printk(KERN_INFO "ip_str_to_hl convert %s to %un", ip_str, ip);
    return  ntohl(ip);
}

static unsigned int hook_func_in(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)
 
{
    static int j=0;
    struct ethhdr *eth;
    struct iphdr *ip_header;
    static unsigned int src_ip;
	static unsigned int dest_ip;
	static unsigned int drop_ip;
	char src_ip_str[16], dest_ip_str[16], drop_ip_str[16];
 
    eth = (struct ethhdr*)skb_mac_header(skb);
    ip_header = (struct iphdr *)skb_network_header(skb);
    //printk(KERN_INFO "src mac %pM, dst mac %pM\n", eth->h_source, eth->h_dest);
    //printk(KERN_INFO "src IP addr: %pI4 ", &ip_header->saddr);
    printk(KERN_INFO "dest IP addr: %pI4\n", &ip_header->daddr);

    /*
    dest_ip =  ip_str_to_hl(str_param);
    ip_hl_to_str(ntohl(dest_ip), dest_ip_str);
    printk(KERN_INFO "%d\n",dest_ip);
   	src_ip = (unsigned int)ip_header->saddr;
    ip_hl_to_str(ntohl(src_ip), src_ip_str);
    printk(KERN_INFO "%d\n",src_ip);
    printk("ip_src = [%s], ip_dest = [%s]\n", src_ip_str, dest_ip_str);
    */
    src_ip = (unsigned int)ip_header->saddr;
    ip_hl_to_str(ntohl(src_ip), src_ip_str);
    dest_ip = (unsigned int)ip_header->daddr;
    ip_hl_to_str(ntohl(dest_ip),dest_ip_str);
    //printk("ip_src = [%s], ip_dest = [%s]\n", src_ip_str, dest_ip_str);
    drop_ip = ip_str_to_hl(str_param);
    ip_hl_to_str(ntohl(drop_ip), drop_ip_str);
    //printk(KERN_INFO "ip_src = [%s]\nip_dest = [%s]\nip_drop = [%s]\n", src_ip_str, dest_ip_str,drop_ip_str);

    if(i==0)
    {
        cancel[i] = drop_ip;
        i++;
    }

    if(drop_ip != cancel[i])
        {
            i++;
            cancel[i]=drop_ip;
        }

    for(j=0;j<=i;j++)
    {
        if(dest_ip == cancel[j])
        {
            printk(KERN_INFO "Drop packet\n");
            printk(KERN_INFO "ip_src = [%s]\nip_dest = [%s]\nip_drop = [%s]\n", src_ip_str, dest_ip_str,drop_ip_str);
            return NF_DROP;
        }
    }
    return NF_ACCEPT;
}

static int __init init_main(void)
{
	printk(KERN_NOTICE "---------------------------------------\nstr_param=%s\n", str_param);
	
    nfin.hook     = hook_func_in;
    nfin.hooknum  = NF_INET_LOCAL_OUT;
    nfin.pf       = PF_INET;
    nfin.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfin);
    
    return 0;
}
 
static void __exit cleanup_main(void)
{
    nf_unregister_net_hook(&init_net, &nfin);
    printk(KERN_INFO "__________________________");
 
}

module_init(init_main);
module_exit(cleanup_main);