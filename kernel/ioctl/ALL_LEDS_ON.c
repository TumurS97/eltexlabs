#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/vt_kern.h>

MODULE_LICENSE("GPL");

static struct timer_list my_timer;
struct tty_driver *my_driver;
char kbledstatus = 0;
#define BLINK_DELAY   HZ/100
#define ALL_LEDS_ON   0x07
#define RESTORE_LEDS  0xFF

void my_timer_callback( struct timer_list *my_timer )
{
  printk( "my_timer_callback called (%ld).\n", jiffies );
  (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
                            ALL_LEDS_ON);
  //my_timer.expires = jiffies + BLINK_DELAY;
}

int init_module( void )
{
  printk("Timer module installing\n");


  my_driver = vc_cons[fg_console].d->port.tty->driver;

  // my_timer.function, my_timer.data
  timer_setup( &my_timer, my_timer_callback, 0 );

  printk( "Starting timer to fire in 200ms (%ld)\n", jiffies );
  //ret = mod_timer( &my_timer, jiffies + msecs_to_jiffies(3000) );
  //if (ret) printk("Error in mod_timer\n");
  my_timer.expires = jiffies + msecs_to_jiffies(1000);
  add_timer(&my_timer);

  return 0;
}

void cleanup_module( void )
{
  int ret;

  ret = del_timer( &my_timer );
  if (ret) printk("The timer is still in use...\n");

  printk("Timer module uninstalling\n");
  (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
                            RESTORE_LEDS);

  return;
}

MODULE_LICENSE("GPL"); 
MODULE_AUTHOR("Dmitrey Salnikov <mr.dimas@meta.ua>"); 

