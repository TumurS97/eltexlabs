#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/vt_kern.h>
#include <linux/moduleparam.h>
#define ALL_LEDS_ON   0x07
#define RESTORE_LEDS  0xFF

MODULE_LICENSE("GPL");

static struct timer_list my_timer;
struct tty_driver *my_driver;

int kbledstatus = 0;
static int int_param;
static int sum=0;

module_param(int_param, int, 0755);

void my_timer_callback( struct timer_list *my_timer )
{ 
  if(sum >= int_param*2)
  {
    (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,RESTORE_LEDS);
    int_param=0;
    sum=0;
    my_timer->expires = jiffies + msecs_to_jiffies(200);
    //printk( "STOPP");
    add_timer(my_timer);
  }
  else
  {
    if (kbledstatus ==0)
    {
      printk( "my_timer_callback ON (%ld).\n", jiffies );
      kbledstatus =1;
      (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,ALL_LEDS_ON);
    }
    else if (kbledstatus ==1)
    {
      printk( "my_timer_callback OF (%ld).\n", jiffies );
      kbledstatus = 0;
      (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,RESTORE_LEDS);
    }

    printk( "summ=%d\n",int_param);
    sum++;
    my_timer->expires = jiffies + msecs_to_jiffies(200);
    add_timer(my_timer);
  }
}


int init_module( void )
{
  printk("Timer module installing param=%d\n",int_param);
  my_driver = vc_cons[fg_console].d->port.tty->driver;
  timer_setup( &my_timer, my_timer_callback, 0 );
  printk( "Starting timer to fire in 200ms (%ld)\n", jiffies );
  my_timer.expires = jiffies + msecs_to_jiffies(200);
  add_timer(&my_timer);
  return 0;
}

void cleanup_module( void )
{
  int ret;
  ret = del_timer( &my_timer );
  if (ret) 
    printk("The timer is still in use...\n");
  printk("Timer module uninstalling\n");
  (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,RESTORE_LEDS);
  return;
}

MODULE_LICENSE("GPL"); 
MODULE_AUTHOR("Dmitrey Salnikov <mr.dimas@meta.ua>"); 

//module_init( init_module ); 
//module_exit( cleanup_module );