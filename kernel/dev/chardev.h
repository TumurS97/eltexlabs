#define DEVICE_NAME "AAA"	
#define BUF_LEN 80				

static int __init mod_init(void);
static void __exit mod_exit(void);

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

MODULE_LICENSE("GPL"); 
MODULE_AUTHOR("timon"); 