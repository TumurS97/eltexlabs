#include <linux/init.h> 
#include <linux/module.h> 
#include <linux/fs.h>
#include <linux/uaccess.h>
#include "chardev.h" 

static int Major;			
static int Device_Open = 0;	
static char msg[BUF_LEN];	
static char *msg_Ptr;

static struct file_operations fops = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};
char data[100];

static int __init mod_init( void ) 
{ 
	printk(KERN_NOTICE "Module loaded!\n" ); 
	Major = register_chrdev(0, DEVICE_NAME, &fops);
	if (Major < 0) 
	{
		printk(KERN_ALERT "Registering char device failed with %d\n", Major);
	  	return Major;
	}
	printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
    return 0; 
}

static void __exit mod_exit( void ) 
{ 
	unregister_chrdev(Major, DEVICE_NAME);
	printk(KERN_NOTICE "Module unloaded!\n" ); 
}

module_init( mod_init ); 
module_exit( mod_exit );

static int device_open(struct inode *inode, struct file *file)
{
	//static int counter = 0;
	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	msg_Ptr = msg;
	try_module_get(THIS_MODULE);
	return 0;
}

static int device_release(struct inode *inode, struct file *file)
{
	Device_Open--;
	module_put(THIS_MODULE);
	return 0;
}

static ssize_t device_read(struct file *filp,char *buffer,size_t length,loff_t * offset)
{
	int bytes_read = 0;
	if (*msg_Ptr == 0)
		return 0;

	while (length && *msg_Ptr) 
	{
		put_user(*(msg_Ptr++), buffer++);
		length--;
		bytes_read++;
	}

	return bytes_read;
}

static ssize_t device_write(struct file *flip, const char __user *buf, size_t count,loff_t *f_pos)
{
	int rv=0;
	printk(KERN_INFO "scull: write to device\n");
	while(count)
	{
		get_user(*(msg_Ptr++), buf++);
		count--;
		rv++;
	}

	return rv;
}
