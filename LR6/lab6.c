#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#define MAX_LEN 50
//Умножение матрицы на вектор. Обработка одной строки матрицы - в порожденном процессе.
int **ReadMas(FILE *fp, int x, int y)
{
	int **mas=NULL;
	mas=malloc(x*sizeof(int*));
	for(int i=0;i<x;i++)
	{
		mas[i]=malloc(y*sizeof(int));
		for(int j=0;j<y;j++)
		fscanf(fp,"%d",&mas[i][j]);
	}
	printf("\n");
	return mas;
}

int multi(int *vector, int *mas,int y)
{
	int num=0;
	for(int i=0;i<y;i++)
	{
		num=num+mas[i]*vector[i];
	}
	return num;
}

void freeMas(int **mas, int x)
{
	free (mas);
	for (int i=0;i<x;i++)
	free (mas[i]);
}

int main(int argc, char **argv)
{
	FILE *fp;
	int fw;
	int x=0,y=0;
	int **mas=NULL;
	if(argc<3)
	{
		printf("Укажите файлы для чтения и записи! ./true.c <file.txt>  <file.txt>\n");
		exit(1);
	}
	
	if ((fp=fopen(argv[1],"r"))==NULL)
	{
		printf("Не удалолось открыть файл!\n");
		exit(1);
	}
	
	/* Читаем данные из файла в массив и вектор */
	fscanf(fp,"%d%d",&x,&y);

	int vector[y];
	mas=ReadMas(fp,x,y);
	printf("You entered matrix: \n\n");
		for(int i=0;i<x;i++)
			{
				for(int j=0;j<y;j++)
					printf("%d  ",mas[i][j]);
					printf("\n\n");
			}
	
		for(int i=0;i<y;i++)
			fscanf(fp,"%d",&vector[i]);
		
		printf("You entered Vector: \n\n");
		for(int i=0;i<y;i++)
				printf("%d\n",vector[i]);
				printf("\n\n");
				
		
		
		fw=open(argv[2], O_RDWR );
		int num=0;
		int pid[x];
		int stat;
		for(int i=0;i<x;i++)
			{
				pid[i] = fork();
				if (pid[i]==-1) 
					{
						perror("fork");
						exit(1);
					}
					
				 else if(pid[i]==0)
					{
						printf("Child [%d]: СТАРТ!\n", i);
						if(lockf(fw, F_LOCK, 0) < 0)
							{
								perror("F_SETLKW");
								exit(0);
							}
							sleep(1);
						printf("\tChild [%d]: locked\n", i);
						num=multi(vector,mas[i],y) ;
						printf("\tChild [%d]: num=%d!\n",i,num);	
					    char buffer[MAX_LEN];
					    sprintf(buffer,"%d\n",num);
						write(fw,buffer,strlen(buffer));
						sleep(1);
						if(lockf(fw, F_ULOCK, 0) <0)
							perror("F_SETLKW");
						printf("\tChild [%d] unlocked\n", i);
						exit(0);
					}		
			}
		 
		 for(int i=0;i<x;i++)
		{
			waitpid(pid[i], &stat, 0);
		}
		 
		 close(fw);
		 if(fclose(fp))
			{ 
				printf("Ошибка при закрытии файла.\n");
				exit(1);
			}
		printf("Результат записан в файл <rez.txt>\n");
		freeMas(mas,x);
			
}
