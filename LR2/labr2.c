#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80

/* Делаю ваиант №5 расположить строки по возрастанию количества слов*/
char** ReadMas(int num)
{
	char buffer[N];
	char **mas;
	mas=malloc(sizeof(char*)*num);
	
	for(int i=0;i<num;i++)
	{
		fgets(buffer,50,stdin);
		mas[i]=malloc(sizeof(char)*strlen(buffer));
		strcpy(mas[i],buffer);
	}
	return mas;	
}

int srav(char ** str1)
{
	char *s;
	s=*str1;
	int scet=0,num=0,i=0;
	num=strlen(s);
	while(i != num)
	{
		if(s[i] == ' ' || s[i]=='\0')
		scet++;
		i++;
	}
	return scet;
	
}

static int compare(const void *p1, const void *p2)
{
	char **str1 = (char **)p1;
	char **str2 = (char **)p2;
	return srav(str1)-srav(str2);
}

void PrintMas(char **mas,int num)
{
	for(int i=0;i<num;i++)
	printf("%s",mas[i]);
}


void freeMas(char **mas, int count)
{
	for (int i = 0; i < count; i++)
	{
        free(mas[i]);
    }
    free(mas);
}

int main(int argc, char *argv[])
{
	//./qw < text.txt
	char **mas=NULL;
	int num=0;
	fscanf(stdin,"%d",&num);
	mas=ReadMas(num);
	qsort(mas,num,sizeof(char **),compare);
	PrintMas(mas,num);
	freeMas(mas,num);		
}
