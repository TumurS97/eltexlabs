#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>     

#define MSGSZ 64  

void DieWithError(char *errorMessage);  
void CreateString(char *pointer,int len);

int main(int argc, char *argv[])
{
    int sock_tcp = 0, sock_udp = 0;

    struct sockaddr_in addrTCP; 
    struct sockaddr_in addrUDP;

       
    
    char buf[MSGSZ];
    int max = MSGSZ-8;  

    if (argc !=3)    
    {
       printf("Usage: <TCP Port> <UDP>\n");
       exit(1);
    }                     

    srand(getpid());

    while(1)
    {   	  
        if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        memset(&addrUDP, 0, sizeof(addrUDP));  

        addrUDP.sin_family = AF_INET;
        addrUDP.sin_port = htons(atoi(argv[1]));
        addrUDP.sin_addr.s_addr = htonl(INADDR_ANY);

        if(bind(sock_udp, (struct sockaddr *)&addrUDP, sizeof(addrUDP)) < 0)
        {
            perror("bind");
            exit(2);
        }

        unsigned int lenth = 0;
        lenth = sizeof(addrUDP);
   
        recvfrom(sock_udp, buf, MSGSZ, 0,  (struct sockaddr *)&addrUDP, &lenth);  

        memset(buf, 0, sizeof(buf));
        printf("%s\n",buf);

	   if ((sock_tcp = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        memset(&addrTCP, 0, sizeof(addrTCP));     

        addrTCP.sin_family      = AF_INET;      
        addrTCP.sin_addr.s_addr = inet_addr(inet_ntoa(addrUDP.sin_addr)); 
        addrTCP.sin_port        = htons(atoi(argv[2]));

        if (connect(sock_tcp, (struct sockaddr *) &addrTCP, sizeof(addrTCP)) < 0)
            DieWithError("connect() failed");

        int len = 0;
        len = rand() % max+1;

        char *pointer = NULL;
        pointer = malloc(sizeof(char)*MSGSZ);
        CreateString(pointer,len);
    
        printf("%s\n",pointer);

        unsigned int StringLen = 0;

        StringLen = strlen(pointer);

        if (send(sock_tcp, pointer, StringLen, 0) != StringLen)
            DieWithError("send() sent a different number of bytes than expected");

        close(sock_tcp);
        close(sock_udp);

        char * pEnd = NULL;
        long int li1 = strtol(pointer, &pEnd, 10);
        free(pointer);

        sleep(li1);

    }    
    exit(0);
}


void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

void CreateString(char *pointer,int len)
{
    char lot[] = "qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM";
    long int size = strlen(lot)-1;
    
    char str[len];
    for(int i=0;i<len;i++)
    {
        str[i] = lot[rand() % size +1];
    }
    str[len] = '\0';
    char newstr[8];
    sprintf(newstr,"%d %ld ",rand() % 9+1,strlen(str));
    strcat(newstr,str);
    newstr[strlen(newstr)] = '\0';
    sprintf(pointer,"%s",newstr);
    newstr[0]='\0';
}
