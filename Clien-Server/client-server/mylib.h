#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h> 
#include <pthread.h> 
#include <sys/msg.h>  
#include <sys/ipc.h>
#include <signal.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#define RCVBUFSIZE 64

struct msgBuffer
{
    long mtype;
    char str[RCVBUFSIZE];
};



struct ThreadArgs
{
    int clntSock,qid;
    struct sockaddr_in addr;
    struct msqid_ds info;
    struct msgBuffer qbuf;                    
};



void DieWithError(char *errorMessage);  
void *RecieveMessageClient(void *arg);
int CreateTCPServerSocket(unsigned short port); 
int CreateUDPServerSocket(char *str,unsigned short udpPort,struct sockaddr_in *addr);
void *ThreadMain1(void *arg);
void *ThreadMain2(void *arg);
void *ThreadAcceptRecieve(void *arg);
int CreateMessageQueue();
void write_message(int qid, struct msgBuffer *qbuf);
void sig_handler(int sig);
void *ThreadAcceptSend(void *threadArgs4);
void *SendMessageClient(void *arg);
char *read_message(int qid,struct msgBuffer *qbuf);

