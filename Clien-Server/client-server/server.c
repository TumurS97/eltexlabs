#include "mylib.h"  

int flag = 0;
pthread_mutex_t mutex;

int main(int argc, char *argv[])
{
    int tcpSock = 0, udpSock = 0;                                    
    int tcpPort = 0, udpPort = 0;

    int tcpSock2 = 0 ,udpSock2 = 0;
    int tcpPort2 = 0, udpPort2 = 0;

    pthread_mutex_init(&mutex,NULL);

    pthread_t threadID;              
    struct ThreadArgs *threadArgs1,*threadArgs2,*threadArgs3,*threadArgs4;
    struct sockaddr_in addr;   

    struct msgBuffer qbuf;
    struct msqid_ds info;


    pthread_t threadWorker, threadSender, threadLast;

    if (signal(SIGINT,sig_handler) == SIG_ERR)
        printf("\ncant catch SIGTERM\n");

    if (argc != 6)    
    {
        fprintf(stderr,"Usage:  %s <SERVER PORT UDP> <SERVER PORT UDP> <TCP RECIEVE> <TCE SEND> <Broacast adress>\n", argv[0]);
        exit(1);
    }

    int qid = CreateMessageQueue();

    udpPort = atoi(argv[1]);       /*Отправляет броадкасты клиентам 1 */        
    tcpPort = atoi(argv[3]);       /*Принимает сообщения по TCP */

    udpPort2 = atoi(argv[2]);       /*Оправляет броадкасты клиентам 2*/
    tcpPort2 = atoi(argv[4]);       /*Отправляет сообщения по TCP */

  

    /*Создадим поток, который будет отправлять UDP сообщения клиентам 1 типа */
    if ((threadArgs1 = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
        DieWithError("malloc() failed");

    memset(&addr, 0, sizeof(addr));
    udpSock  = CreateUDPServerSocket(argv[5],udpPort,&addr);

    threadArgs1 -> clntSock = udpSock;
    threadArgs1 -> addr = addr;
    threadArgs1 -> info = info;
    threadArgs1 -> qid = qid;

    if (pthread_create(&threadWorker,NULL,ThreadMain1,(void *) threadArgs1) !=0)
        DieWithError("malloc() failed");


    /*Создадим поток,который будет принимать TCP сообщения от клиентов и писать их в ОС */
    
    tcpSock = CreateTCPServerSocket(tcpPort);

    if ((threadArgs2 = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
        DieWithError("malloc() failed");

    threadArgs2 -> clntSock = tcpSock;
    threadArgs2 -> qid = qid;
    threadArgs2 -> info = info;
    threadArgs2 -> qbuf = qbuf;

    if (pthread_create(&threadID, NULL, ThreadAcceptRecieve, (void *) threadArgs2) != 0)
        DieWithError("pthread_create() failed");

    /*Создадим поток, который будет отправлять UDP сообщения клиентам 2 типа */

     if ((threadArgs3 = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
        DieWithError("malloc() failed");

    memset(&addr, 0, sizeof(addr));
    udpSock2  = CreateUDPServerSocket(argv[5],udpPort2,&addr);

    threadArgs3 -> clntSock = udpSock2;
    threadArgs3 -> addr = addr;
    threadArgs3 -> info = info;
    threadArgs3 -> qid = qid;

    if (pthread_create(&threadSender,NULL,ThreadMain2,(void *) threadArgs3) !=0)
        DieWithError("malloc() failed");

    /*Создадим поток, который будет отправлять TCP сообщения клиентам 2 типа */
    tcpSock2 = CreateTCPServerSocket(tcpPort2);

    if ((threadArgs4 = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
        DieWithError("malloc() failed");

    threadArgs4 -> clntSock = tcpSock2;
    threadArgs4 -> qid = qid;
    threadArgs4 -> info = info;
    threadArgs4 -> qbuf = qbuf;

    if (pthread_create(&threadLast, NULL, ThreadAcceptSend, (void *) threadArgs4) != 0)
        DieWithError("pthread_create() failed");


    while(1)
    {
        if(flag == 1)
            break;
        sleep(1);
    }

    pthread_mutex_destroy(&mutex);

    if( (msgctl(qid, IPC_RMID, NULL)) == -1)
    {
        printf("msgctl_error\n");
        exit(1);
    }
}


void *ThreadAcceptRecieve(void *threadArgs)
{
    int servSock = ((struct ThreadArgs *) threadArgs) -> clntSock;
    int qid = ((struct ThreadArgs *) threadArgs) -> qid;
    struct msqid_ds info = ((struct ThreadArgs *) threadArgs) -> info;
    struct msgBuffer qbuf = ((struct ThreadArgs *) threadArgs) -> qbuf;

    free(threadArgs);  
    pthread_detach(pthread_self()); 
    struct ThreadArgs *arg;

    pthread_t potok;

    while(1)
    {
        if ((arg = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
            DieWithError("malloc() failed");

        int clntSock=0;  

        if ((clntSock = accept(servSock,NULL, NULL)) < 0)
        DieWithError("accept() failed");
        arg -> clntSock = clntSock;
        arg -> qid = qid;
        arg -> info = info;
        arg -> qbuf = qbuf;

        if (pthread_create(&potok, NULL, RecieveMessageClient, (void *) arg) != 0)
            DieWithError("pthread_create() failed");
    }

    return (NULL);
}
void *RecieveMessageClient(void *arg)
{
    struct ThreadArgs*  threadArgs =(struct ThreadArgs *) arg;
    struct ThreadArgs myStruct = *threadArgs;
    free(arg);

    int Sock = 0;
    Sock = myStruct.clntSock;
    pthread_detach(pthread_self());

    char echoBuffer[RCVBUFSIZE];
    memset(echoBuffer, 0, sizeof(echoBuffer));
    int recvMsgSize=0;

    if ((recvMsgSize = recv(Sock, echoBuffer, RCVBUFSIZE, 0)) < 0)
        DieWithError("recv() failed");


    printf("%s\n",echoBuffer);

    strcpy(myStruct.qbuf.str, echoBuffer);
    myStruct.qbuf.mtype=1;

    //pthread_mutex_lock(&mutex);
    write_message(myStruct.qid,&myStruct.qbuf);
    //pthread_mutex_unlock(&mutex);

    close(Sock);
    return NULL;
}



void *ThreadAcceptSend(void *threadArgs4)
{
    int servSock = ((struct ThreadArgs *) threadArgs4) -> clntSock;
    int qid = ((struct ThreadArgs *) threadArgs4) -> qid;
    struct msqid_ds info = ((struct ThreadArgs *) threadArgs4) -> info;
    struct msgBuffer qbuf = ((struct ThreadArgs *) threadArgs4) -> qbuf;

    free(threadArgs4);  
    pthread_detach(pthread_self()); 
    struct ThreadArgs *arg;

    pthread_t potok;

    while(1)
    {
        if ((arg = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL)
            DieWithError("malloc() failed");

        int clntSock=0;  

        if ((clntSock = accept(servSock,NULL, NULL)) < 0)
        DieWithError("accept() failed");
        arg -> clntSock = clntSock;
        arg -> qid = qid;
        arg -> info = info;
        arg -> qbuf = qbuf;

        if (pthread_create(&potok, NULL, SendMessageClient, (void *) arg) != 0)
            DieWithError("pthread_create() failed");
    }

    return (NULL);
}
void *SendMessageClient(void *arg)
{
    struct ThreadArgs*  threadArgs =(struct ThreadArgs *) arg;
    struct ThreadArgs myStruct = *threadArgs;
    free(arg);
    pthread_detach(pthread_self());
    int sock = myStruct.clntSock;
    char buf[RCVBUFSIZE];

    pthread_mutex_lock(&mutex);
    sprintf(buf,"%s",read_message(myStruct.qid,&myStruct.qbuf));

    pthread_mutex_unlock(&mutex);

    int echoStringLen = strlen(buf);

    if (send(sock, buf, echoStringLen, 0) != echoStringLen)
        DieWithError("send() sent a different number of bytes than expected");

    close(sock);
    return NULL;
}


void sig_handler(int sig)
{
    if(sig == SIGINT)
    {
        printf("received SIGINT\n");
        flag = 1;    
    }
}



