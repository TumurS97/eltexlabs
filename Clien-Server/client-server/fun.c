#include "mylib.h"  


#define MAXPENDING 5   
#define MAX_QUEUE  10

char msg1[] = "Жду сообщений";
char msg2[] = "Готов к отправке";

int CreateTCPServerSocket(unsigned short port)
{
    int sock;                        /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */

    /* Create socket for incoming connections */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");
      
    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(port);              /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind() failed");

    /* Mark the socket so it will listen for incoming connections */
    if (listen(sock, MAXPENDING) < 0)
        DieWithError("listen() failed");

    return sock;
}

int CreateUDPServerSocket(char str[],unsigned short udpPort,struct sockaddr_in *addr)
{
    struct hostent *he;
    int sock;  
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    //char mas[]="192.168.0.255";
    if ((he=gethostbyname(str)) == NULL) 
    {  // получаем инфо о хосте
        perror("gethostbyname");
        exit(1);
    }


  
    int n=1;
    setsockopt(sock,SOL_SOCKET,SO_BROADCAST,&n,sizeof(n));

    addr -> sin_family = AF_INET;
    addr -> sin_port = htons(udpPort);
    //addr -> sin_addr.s_addr = inet_addr("192.168.0.255"); 
    addr -> sin_addr = *((struct in_addr *)he->h_addr);
    return sock;

}

void *ThreadMain1(void *arg)
{
    pthread_detach(pthread_self()); 
    struct ThreadArgs*  threadArgs =(struct ThreadArgs *) arg;
    struct ThreadArgs myStruct = *threadArgs;
    free(arg);  

    int main_port = ntohs(myStruct.addr.sin_port);

    while(1)
    {
        if( (msgctl(myStruct.qid, IPC_STAT, &myStruct.info)) == -1)
            DieWithError("msgctl_error");

        if (myStruct.info.msg_qnum < MAX_QUEUE)
        {
            for(int i=0;i<10;i++)
            {
                sendto(myStruct.clntSock,msg1,sizeof(msg1),0,(struct sockaddr*)&myStruct.addr,sizeof(myStruct.addr));
                myStruct.addr.sin_port = htons(main_port+i);
            }
            myStruct.addr.sin_port = htons(main_port);
            sleep(5);
        }
        else
            sleep(1);
    }
    return NULL;
}

void *ThreadMain2(void *arg)
{
    pthread_detach(pthread_self()); 
    struct ThreadArgs*  threadArgs =(struct ThreadArgs *) arg;
    struct ThreadArgs myStruct = *threadArgs;
    free(arg);  

    int main_port = ntohs(myStruct.addr.sin_port);

    while(1)
    {
        if( (msgctl(myStruct.qid, IPC_STAT, &myStruct.info)) == -1)
            DieWithError("msgctl_error");

        if (myStruct.info.msg_qnum >= 1 )
        {
            for(int i=0;i<10;i++)
            {
                sendto(myStruct.clntSock,msg1,sizeof(msg1),0,(struct sockaddr*)&myStruct.addr,sizeof(myStruct.addr));
                myStruct.addr.sin_port = htons(main_port+i);
            }
            myStruct.addr.sin_port = htons(main_port);
            sleep(2);
        }
        else
            sleep(1);
    }
    return NULL;
}




void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

int CreateMessageQueue()
{
    int qid;
    key_t key;
    key = ftok(".",'m');

    if((qid = msgget(key, IPC_CREAT | 0666))==-1)
        DieWithError("msgget");

    return qid;
}

void write_message(int qid, struct msgBuffer *qbuf)
{
    if((msgsnd(qid, (struct msgbuf *)qbuf,strlen(qbuf->str)+1, 0)) ==-1)
    {
        perror("msgsnd");
        exit(1);
    }
}

char *read_message(int qid,struct msgBuffer *qbuf)
{
    int type = qbuf->mtype;
    msgrcv(qid, (struct msgbuf *)qbuf, RCVBUFSIZE,type ,0);
    char *string=NULL;
    string = qbuf->str;
    
    return (string);
}



