#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <string.h>     
#include <unistd.h>
#include "amessage.pb-c.h"
#define RCVBUFSIZE 64 
#define MAX_MSG_SIZE 1024

void DieWithError(char *errorMessage);  

int main(int argc, char *argv[])
{
    int sock_tcp = 0, sock_udp = 0;
    struct sockaddr_in addrTCP; 
    struct sockaddr_in addrUDP;
    AMessage *msg;
    uint8_t buffer[MAX_MSG_SIZE];
    size_t bytes_read;

       
    
    char buf[RCVBUFSIZE];

    if (argc !=3)    
    {
       printf("Usage: <TCP Port> <UDP>\n");
       exit(1);
    }                     

    srand(getpid());

while(1)
{
     if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

        memset(&addrUDP, 0, sizeof(addrUDP));  

        addrUDP.sin_family = AF_INET;
        addrUDP.sin_port = htons(atoi(argv[1]));
        addrUDP.sin_addr.s_addr = htonl(INADDR_ANY);

        if(bind(sock_udp, (struct sockaddr *)&addrUDP, sizeof(addrUDP)) < 0)
        {
            perror("bind");
            exit(2);
        }

        unsigned int lenth = 0;
        lenth = sizeof(addrUDP);


         memset(&addrTCP, 0, sizeof(addrTCP));
      
        recvfrom(sock_udp, buf, RCVBUFSIZE, 0,  (struct sockaddr *)&addrUDP, &lenth);  
        //printf("IP server: %s\n",inet_ntoa(addrUDP.sin_addr));
        memset(buf, 0, sizeof(buf));
        addrTCP.sin_family      = AF_INET;      
        addrTCP.sin_addr.s_addr = inet_addr(inet_ntoa(addrUDP.sin_addr)); 
        addrTCP.sin_port        = htons(atoi(argv[2]));


        memset(buf, 0, sizeof(buf));
        

       if ((sock_tcp = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");
 
        if (connect(sock_tcp, (struct sockaddr *) &addrTCP, sizeof(addrTCP)) < 0)
            DieWithError("connect() failed");

        //char echoBuffer[RCVBUFSIZE];
        //memset(echoBuffer, 0, sizeof(echoBuffer));
        //int recvMsgSize=0;

        bytes_read= recv(sock_tcp, buffer, MAX_MSG_SIZE, 0);
        msg = amessage__unpack(NULL, bytes_read, buffer);   
        if (msg == NULL)
        {
            fprintf(stderr, "error unpacking incoming message\n");
            exit(1);
        }

        printf("Recieved:\n\tstring = %s\n\ttime = %d\n\tlen = %d\n",msg->c,msg->a,msg->b);
        amessage__free_unpacked(msg, NULL);

        close(sock_tcp);
        close(sock_udp);
        sleep(msg->a);

    }    
    exit(0);
}


void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}
