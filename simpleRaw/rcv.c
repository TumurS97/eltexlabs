#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <unistd.h>

/*mac-адрес клиента*/
#define DEST_MAC0	0x00
#define DEST_MAC1	0x25
#define DEST_MAC2	0x22
#define DEST_MAC3	0x60
#define DEST_MAC4	0x78
#define DEST_MAC5	0xf6

#define ETHER_TYPE	0x0800
#define DEFAULT_IF	"wlp1s0"
//#define DEFAULT_IF	"enp1s0"

#define BUF_SIZ		1024

int main(int argc, char *argv[])
{
	int sockfd;
	int sockopt;
	int numbytes;

	struct ifreq if_mac;

	uint8_t buf[BUF_SIZ];
	char ifName[IFNAMSIZ];
	struct ether_header *eh = (struct ether_header *) buf;
	strcpy(ifName, DEFAULT_IF);

	/* Open PF_PACKET socket, listening for EtherType ETHER_TYPE */
	if ((sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) 
	{
		perror("listener: socket");	
		return -1;
	}

	/* Allow the socket to be reused - incase connection is closed prematurely */
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1) {
		perror("setsockopt");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	/* Bind to device */
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)	{
		perror("SO_BINDTODEVICE");
		close(sockfd);
		exit(EXIT_FAILURE);
	}



	memset(&if_mac,0,sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");

	printf("My mac : ");
	for(int i=0;i<6;i++)
	{	
		printf("%02x-",(unsigned char) if_mac.ifr_hwaddr.sa_data[i]);
	}
	printf("\n\n");

	int len = sizeof(struct ether_header);
		memset(buf, 0, BUF_SIZ);

	while(1)
	{
		numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, NULL, NULL);
		//printf("numbytes = %d\n",numbytes);
		/* Check the packet is for me */
		if (eh->ether_shost[0] == DEST_MAC0 &&
			eh->ether_shost[1] == DEST_MAC1 &&
			eh->ether_shost[2] == DEST_MAC2 &&
			eh->ether_shost[3] == DEST_MAC3 &&
			eh->ether_shost[4] == DEST_MAC4 &&
			eh->ether_shost[5] == DEST_MAC5) 
		{
			printf("Recieve:\n\t");
			printf("Correct destination MAC address\n");
			printf("\tPacket %d bytes\n", numbytes);
			/* Print packet */
			printf("\tData:");

			for (int i=len; i<numbytes+len; i++)
			{ 
				printf("%c", buf[i]);
				if(buf[i]==0)
					break;
			}
			printf("\n");
			//Отвечаем нашему клиенту...

			
		} 

	}

}