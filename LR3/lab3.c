#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 80
// 7 variant
typedef struct student
{
	char *family;
	char *group;
	int number;
	int stipend;	
}student;

student *ScanStructur(int num)
{
	student *p=NULL;
	char buffer[MAX_LEN];
	p= malloc(num * sizeof(student));
	for(int i=0;i<num;i++)
	{
		fscanf(stdin,"%s",buffer);
		p[i].family=malloc(sizeof(char)*strlen(buffer));
		strcpy(p[i].family,buffer);
		fscanf(stdin,"%s",buffer);
		p[i].group=malloc(sizeof(char)*strlen(buffer));
		strcpy(p[i].group,buffer);
		fscanf(stdin,"%d",&p[i].number);
	    fscanf(stdin,"%d",&p[i].stipend);	
	}
	
	return p;
}

void PrintStruct(student *p)
{
	printf("\nФамилия:   %s\nГруппа:    %s\nНомер:     %d\nСтипендия: %d\n",
	p->family,p->group,p->number,p->stipend);
	
}

void freeStruct(student **p, int num)
{
	for(int i=0;i<num;i++)
	{
		free((*p)[i].family);
		free((*p)[i].group);
	}
	free(*p);
}

static int compare(const void *p1, const void *p2)
{
	const student* a = (const student *)p1;
	const student* b = (const student *)p2;
	
	return ((a->number) - (b->number));
}



int main (int argc, char *argv[])
{
	int num=5;
	student *p=NULL;
	p=ScanStructur(num);
	for(int i=0; i<num;i++)
		PrintStruct(&p[i]);	
	qsort(p,num, sizeof(student),compare);
	printf("\n\nПосле сортировкки  по номеру\n");
	for(int i=0; i<num;i++)
		PrintStruct(&p[i]);	
	
	freeStruct(&p, num);
		
}
