#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#define Max 10

char message[] = "zapros_massiva\n";
char message3[]="THE_END";


struct ThreadArgs
{
    int ClientSocket;
    int mas[Max][Max],sizeMas, NumStr; //sizeMas = [x][x]
};


int main()
{
    int sock;
    struct sockaddr_in addr;
    struct ThreadArgs threadArgs;

    char buffer[1024];




    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0)
    {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(3426); // или любой другой порт...
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("connect");
        exit(2);
    }

    send(sock, message, sizeof(message), 0);
    //recv(sock, buffer, sizeof(message), 0);
    
    recv(sock,buffer,sizeof(buffer),0);

    memcpy(&threadArgs,buffer,sizeof(buffer));

    printf("STRUCT sizeMas=%d\nNumStr=%d\n",threadArgs.sizeMas,threadArgs.NumStr);

    for(int i=0;i<threadArgs.sizeMas;i++)
      {
          for(int j=0;j<threadArgs.sizeMas;j++)
          {
              printf("%d  ",threadArgs.mas[i][j]);
          }
          printf("\n");
      }
      


      for(int i=0;i<threadArgs.sizeMas;i++)
      {
        if (threadArgs.mas[threadArgs.NumStr][i] == 1)
        {
            sprintf(buffer,"%d  %d",threadArgs.NumStr,i);
            printf("Target: [%d][%d]\n",threadArgs.NumStr,i);
            send(sock,buffer,sizeof(buffer),0);
        }
        sleep(1);
      }



    send(sock, message3, sizeof(message3), 0);

    close(sock);

    return 0;
}