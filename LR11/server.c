#include <stdio.h>      
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <stdlib.h>     
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#define Max 10
//  Авиаразведка, траектории  случайные
void *ThreadMain(void *arg);
char message3[]="THE_END";
pthread_mutex_t mutex;

struct ThreadArgs
{
	int ClientSocket;
	int mas[Max][Max],sizeMas, NumStr; //sizeMas = [x][x]
};

typedef struct ThreadArgs  thread; 

int main()
{

	pthread_mutex_init(&mutex,NULL);
    int sock, listener;
    struct sockaddr_in addr;
    thread threadArgs;
    pthread_t threadID;

    //создание сокета
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3426);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, 1);

    //Создаем массив и заполняем
     int x=5;
    threadArgs.sizeMas=x;
 
    for(int i=0;i<x;i++)
    for(int j=0;j<x;j++)
    	threadArgs.mas[i][j] = 0;

    srand(getpid());

    for(int i=0;i<x*x/2;i++)
    	threadArgs.mas[(rand() % x +0)][(rand() % x +0)] = 1;

    printf("\nИсходное поле:\n\n");
    for(int i=0;i<x;i++)
    {
    	for(int j=0;j<x;j++)
    	{
    		printf("%d  ",threadArgs.mas[i][j]);
    	}
    	printf("\n");
    }
    printf("\n");

    while(1)
    {
        sock = accept(listener, NULL, NULL);
        if(sock < 0)
        {
            perror("accept");
            exit(3);
        }

        threadArgs.ClientSocket = sock;
        threadArgs.NumStr = (rand() % x +0);

    	if (pthread_create(&threadID,NULL,ThreadMain,&threadArgs) !=0)
    	{
    		perror("pthread_create");
    		exit(4);
   	 	}	
    	//printf("Pthread create: %ld\n",(long int) threadID);
    }


    pthread_mutex_destroy(&mutex);
    
    return 0;

}

void *ThreadMain(void *arg)
{
	pthread_detach(pthread_self());
	pthread_mutex_lock(&mutex);
	thread *threadArgs = (thread *) arg;
	thread myStruct = *threadArgs;

	int sock = myStruct.ClientSocket;
	char buffer[1024];

	recv(sock,buffer,sizeof(buffer),0);
	//printf("%s\n",buffer);

	memcpy(buffer,&myStruct,sizeof(myStruct));

	send(sock,buffer,sizeof(buffer),0);
	pthread_mutex_unlock(&mutex);

	while(1)
	{
		recv(sock,buffer,sizeof(buffer),0);

		if (strcmp(buffer,message3) == 0)
		{
			//printf("Обмен закончен \n");
			close(sock);  
			return NULL;
		}
		//выводим координаты целей на экран
		printf("Поток %ld : цель : [%s]\n",pthread_self(),buffer);
	}


	close(sock);
	sleep(1);
	return NULL;
} 