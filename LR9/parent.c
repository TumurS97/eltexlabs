#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>

union semun
{
	int val;                /*Значение для SETVAL*/
	struct semid_ds *buf;   /*Буферы для IPC_STAT, IPC_SET  */ 
	unsigned short *array;  /*Массивы для GETALL, SETALL */
	struct seminfo *__buf;   /*Буфер для IPC_INFO */
};

int LockMem(int semid,struct sembuf *lock_res, int status )
{
	if ((semop(semid,lock_res,status))==-1 )
	{
		return(-1);
	}
	return(0);
}

int UnlockMem(int semid, struct sembuf *rel_res, int status)
{
	if((semop(semid, rel_res, status)) == -1)
		{
			return(-1);
		}
	return(0);
}

int main (int argc, char **argv)
{
	if( argc != 5 )
	{
		printf("Используйте ./parent <Порция мёда (ед.)> <Время (сек.)> <Кол-во пчел>  <Шаг добычи> \n");
		exit(1);
	}
	
	int copy = fork();
	
	if (copy == 0)
	{
		execl("./child",argv[3],argv[4],NULL);
	}
	
	int Med,Time;
	Med=atoi(argv[1]);
	Time=atoi(argv[2]);
	printf(" Parents: Med = %d; Time = %d;\n\n",Med,Time);
	
	key_t key;
	key = ftok(".",'m');
	
	union semun arg;
	struct sembuf lock_res = {0,-1,0}; //Блокировка ресурса	
	struct sembuf rel_res = {0,1,0};   //Освобождение ресурса
	
	/* Создадим семафор */
	int semid,shmid;
	int size = sizeof(int);
	int *s;
	
	semid = semget(key,1, 0666 | IPC_CREAT);
	
	/*Установим в семафоре N 0 значение 1 */
	 arg.val =1;
	 semctl(semid,0,SETVAL,arg);
	 
	/* Создадим область разделяемой памяти */
    if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0) 
		{
			perror("shmget\n");
			exit(1);
		}
	
	  /* Получим доступ к разделяемой памяти */
    if ((s = shmat(shmid, NULL, 0)) == (int *) -1) 
		{
			perror("shmat");
			exit(1);
		}	
	
	sleep(Time);
	while(1)
	{
		if (*s<Med)
			{	printf(" Vinni: Мёда недостаточно жду и умираю!\n\n");
				sleep(Time);
			}
		
		if (*s<Med)
			{	
				printf(" Vinni: Я умер!\n");
				break;
			}
			
		/* Заблокируем разделяемую память*/
		if (  (LockMem(semid,&lock_res,1)) == -1  )
			{
				printf("Lock failed\n");
				exit(1);
			}	
		
		printf(" Vinni: Всего [%d] Забрал [%d] Осталось [%d]\n",*s,Med,*s-Med);
		
		*s=*s-Med;
	
		/* Разблокируем разделяемую память */
		if ( (UnlockMem(semid,&rel_res,1)) == -1  )
			{
				printf("Unlock failed\n");
				exit(1);
			}	
		printf(" Vinni: Ем мёд\n\n");
		sleep(Time);
	
	}
	
	/* То есть если винни умер */
	kill(copy, SIGTERM);
	
	int stat,status;
	status = waitpid(copy, &stat, 0);
	if (copy == status) 
	{
		printf("Unit %d done,  result=%d\n", copy, WEXITSTATUS(stat));
	}
		
	if (shmctl(shmid, IPC_RMID, 0) < 0) 
	{
		//printf("Невозможно удалить область\n");
		exit(1);
	}
	 
	if (semctl(semid, 0, IPC_RMID) < 0) 
	{
		//printf("Невозможно удалить семафор\n");
		exit(1);
	}
	
}
