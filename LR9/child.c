#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
//Винни-Пух и пчелы.
int m=0;

union semun
{
	int val;                /*Значение для SETVAL*/
	struct semid_ds *buf;   /*Буферы для IPC_STAT, IPC_SET  */ 
	unsigned short *array;  /*Массивы для GETALL, SETALL */
	
	struct seminfo *__buf;   /*Буфер для IPC_INFO */
};

void sig_handler(int sig)
{
	if(sig == SIGTERM)
	{
		printf("received SIGTERM\n");
		m=1;	
	}
	
	if(sig == SIGINT)
	{
			printf("received SIGINT\n");
		exit(0);
		
	}
}

int main (int argc, char **argv)
{
	if(signal(SIGTERM,sig_handler) == SIG_ERR)
		printf("\ncant catch SIGTERM\n");

	if(signal(SIGINT,sig_handler) == SIG_ERR)
		printf("\ncant catch SIGTERM\n");
	
	int step,count;
	count=atoi(argv[0]);
	step=atoi(argv[1]);
	printf(" Child: count = %d; step = %d;\n",count,step);
	
	key_t key;
	key = ftok(".",'m');

	struct sembuf lock_res = {0,-1,0}; //Блокировка ресурса	
	struct sembuf rel_res = {0,1,0};   //Освобождение ресурса
	
	/* Получаем id семафора */
	int semid,shmid;
	int size = sizeof(int);
	int *s;
	semid = semget(key,1, 0666 | IPC_CREAT);
	
	/* id памяти */
    if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0) 
		{
			perror("shmget\n");
			exit(1);
		}
    
    // Получим доступ к разделяемой памяти 
    if ((s = shmat(shmid, NULL, 0)) == (int *) -1) 
		{
			perror("shmat");
			exit(1);
		}
	
	int pid[count];
	
	 for(int i=1;i<=count;i++)
	 {
		pid[i]=fork();
		if(pid[i]==0)
		{
			while(1)
			{	
				sleep (rand() % 7 +1);
				//Заблокируем разделяемую память 
				if ((semop(semid,&lock_res,1)) ==-1)
				{
					printf("Lock failed\n");
					exit(1);
				}
				else
					printf("Bee[%d]=Меда в сумме = %d \n",i,*s+step);
				*s=*s+step;
				// Разблокируем разделяемую память 
				if((semop(semid, &rel_res, 1)) == -1)
				{
					printf("Unlock failed\n");
					exit(1);
				}
			}
		}
	 }	
	
	while(m==0);
		sleep(1);
		
	for(int i=1;i<= count;i++)
	kill(pid[i],SIGINT);
		
	int stat,status;
		for (int i = 1; i <= count; i++) 
		{
			status = waitpid(pid[i], &stat, 0);
			if (pid[i] == status) 
			{
				printf("Unit %d done,  result=%d\n", i, WEXITSTATUS(stat));
			}
		}
		
		/* Удалим созданные объекты IPC */	
	 if (shmctl(shmid, IPC_RMID, 0) < 0) 
		{
			printf("Невозможно удалить область\n");
			exit(1);
		}
	 
	 if (semctl(semid, 0, IPC_RMID) < 0) 
		{
			printf("Невозможно удалить семафор\n");
			exit(1);
		}
	printf("Конец\n");
	return(0);
}
