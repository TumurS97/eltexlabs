#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define Len 128
 /*Warcraft. Заданное количество юнитов добывают золото равными порциями из одной шахты, 
 задерживаясь в пути на случайное время, до ее истощения. 
 Работа каждого юнита реализуется в порожденном процессе.*/

struct mymsgbuf
{
	long mtype;
	int gg;
};

int open_queue(key_t key)
{
	int qid;
	if((qid = msgget(key, IPC_CREAT | 0666))==-1)
		{
			return (-1);
		}

	return(qid);
}

int send_message(int qid, struct mymsgbuf *qbuf)
{
	int result, length;
	
	length= sizeof(struct mymsgbuf)- sizeof(int);
	
	if  ( (result = msgsnd(qid,qbuf,length,IPC_NOWAIT)) ==-1)
	{
		return (-1);
	}
	
	return result;
}

int read_message(int qid, struct mymsgbuf *qbuf)
{
	int result, length;
	length= sizeof(struct mymsgbuf)- sizeof(int);
	
	if((result=msgrcv(qid,qbuf,length,1,0))==-1)
	{
		return (-1);
	}
	return (result);
}

int Procces(int qid,int i,char * str,struct mymsgbuf *qbuf)
{
	int sum;   		   // кол-во ресов
	int it=0;  		   //сколько добыл в сумме
	int z=atoi(str);   //шаг добычи
	while(1)
	{
		sleep (rand() % 3 +1); //дорога до шахты
		if ((read_message(qid,qbuf)) ==-1)
			{
				perror("read_message error\n");
				exit(0);
			}
		
		sum=qbuf->gg;
		
		if(sum <= 0)
			{
				qbuf->gg=0;
				if((send_message(qid,qbuf)) ==-1)
					{
						perror("send_message error\n");
						exit(1);
					}	
				return(it);
			}
		else if(sum < z)
			z=sum;
			
		printf("Юнит n = [%d] было [%d] осталось [%d]\n",i,sum,(sum-z));
		sum=sum-z;
		qbuf->gg=sum;

		if((send_message(qid,qbuf)) ==-1)
			{
				perror("send_message error\n");
				exit(1);
			}	
		it=it+z;
	}
}

int main (int argc, char **argv)
{
	struct mymsgbuf qbuf;
	if(argc!=4)
		{
			printf(" Ошибка, используй ./think.c <Число юнитов> <Кол-во рес-ов> <Шаг добычи> \n");
			exit(1);
		}
	
	qbuf.gg = atoi(argv[2]);
	qbuf.mtype=1;
	
	if(atoi(argv[3]) <= 0)
		{
			printf("Шаг добычи должен быть > 0 \n");
			exit(1);
		}
	printf("arg=%d\n",atoi(argv[3]));
	
	int qid;
	key_t key;
	
	key = ftok(".",'m');
	
	/*открываем очередь */
	if((qid = open_queue(key)) ==-1)
		{
			perror("open_queue error\n");
			exit(1);
		}

	/* Пишим в очередь изначальное значение*/

	if((send_message(qid,&qbuf)) ==-1)
		{
			perror("send_message error\n");
			exit(1);
		}

	/*  Реализация юнитов   в  нескольких процессах */

	int x=atoi(argv[1]);
	int pid[x];

	for(int i=1;i<=x;i++)
		{
			pid[i] = fork();
			if (pid[i]==-1) 
				{
					perror("fork error\n");
					exit(1);
				}
			else if(pid[i]==0)
				{
					qbuf.gg=Procces(qid,i,argv[3],&qbuf);
					qbuf.mtype=10+i;
				
					if((send_message(qid,&qbuf)) ==-1)
						{
							perror("send_message");
							exit(1);
						}
					exit(0);
				}	
		}
	
	int stat,status;
	
	for (int i = 1; i <= x; i++) 
	{
	
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) 
        {
            printf("Unit %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }
    
    printf("\n");
    int result, length;
    /* Читаем из очереди результат каждого процесса с уникальным типом */
    for(int i=1;i<=x;i++)
    {
		length= sizeof(struct mymsgbuf)- sizeof(int);
	
		if((result=msgrcv(qid,&qbuf,length,10+i,0))==-1)
		{
			return (-1);
		}
		printf("Unit N = %d | summ = %d\n",i,qbuf.gg);
	}
	
	if( (msgctl(qid, IPC_RMID, NULL)) == -1)
	{
		printf("msgctl_error\n");
		exit(1);
	}
	//для удаления из консоли ipcrm msg <id-number>
}


