#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	if (argc !=2)
	{
		printf("Usage: ./main <del or umn>\n");
		exit(1);
	}
	
	void *ext_library;
	int value=0;
	int value2=0;
	int (*powerfunc)(int x,int y);
	printf("Введи первое число\n");
	scanf("%d",&value);
	
	while(value2 ==0)
	{
		printf("\nВведи второе число\n");
		scanf("%d",&value2);
		if(value2==0)
		printf("На ноль делить нежелательно\n");
	}
	//ext_library = dlopen("/home/timon/libs/libpowers.so", RTLD_LAZY);
	ext_library = dlopen("./libpowers.so", RTLD_LAZY);
	if (!ext_library)
	{
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		return 1;
	}
	powerfunc = dlsym(ext_library, argv[1]);
	printf("%s = %d\n",argv[1],(*powerfunc)(value, value2));
	
	dlclose(ext_library);
	
	
}
